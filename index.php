<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit', '128M');

include 'simple_HTML_dom.php';
include 'simple_image.php';
include 'Db.php';

class Recipe
{
	
	private function save($table, $parameters)
	{
		$db = new Db;
		
		foreach($parameters as $key=>$value)
		{
			$params[] = ':'.$key;
			$columns[] = $key;
			$bindParams[':'.$key] = $value;
		}
		 
		$db->query("INSERT INTO ".$table." (".implode(", ", $columns).") VALUES (".implode(', ', $params).")");
		
		return $db->params($bindParams)->result();
	}
	
	private function find($table, $parameters)
	{
		$db = new Db;
		
		foreach($parameters as $key => $value)
        {
            $params[] = $key." = ?";
            $bindParams[] = $value; 
        }
        
        $db->query("SELECT * FROM ".$table." WHERE ".implode(' AND ', $params))->params($bindParams); 
		
		$result = $db->result(\PDO::FETCH_ASSOC);
        
        return $result;
	}
	
	public function run()
	{
		//recipe
		$recipe = $this->find('recipes', ['name'=>$this->name]);
		if($recipe)
			return false;
		
		$recipe_id = $this->save('recipes', ['name'=>$this->name, 'descr'=>$this->descr, 'img'=>$this->img]);
		
		//category
		foreach($this->categories as $item)
		{
			$category = $this->find('categories', ['name'=>$item['value']]);
			if($category)
				$category_id = $category[0]['id'];
			else
				$category_id = $this->save('categories', ['name'=>$item['value']]);
			
			if($recipe_id)
				$this->save('recipes_categories', ['category_id'=>$category_id,'recipe_id'=>$recipe_id]);
		}
		
		//tags
		
		foreach($this->ingredients as $item)
		{
			$tag = $this->find('tags', ['value'=>strip_tags($item['value'])]);
			
			if($tag)
				$this->save('recipes_tags', ['tag_id'=>$tag[0]['id'],'recipe_id'=>$recipe_id,'count'=>$item['count']]);
			else
			{
				$tag = $this->save('tags', ['value'=>strip_tags($item['value'])]);
				$this->save('recipes_tags', ['tag_id'=>$tag,'recipe_id'=>$recipe_id,'count'=>$item['count']]);
			}
		}
		
	}
}

$paginator = true;
$linkNext = 'http://eda.ru/recipes';
//$linkNext = 'http://eda.ru/recipelist/page132';

$recipesCount = 0;

while($paginator)
{
	$html = file_get_html($linkNext);

	$recipes = $html->find('.b-recipe-widget');
	
	for($i = 0; $i < count($recipes); $i++)
	{
		$recipeLink = $recipes[$i]->find('a')[0]->href;
		$recipeHtml = file_get_html($recipeLink);
		
		$img = $recipeHtml->find('#s-photoListContainer img')[0]->src;
		
		$Rec = new Recipe();
		
		$Rec->img = '';
		
		if($img)
		{
			$name = md5(uniqid()).'.jpg';
			$path = dirname(__FILE__).'/img/'.$name;			
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $img);
			curl_setopt($ch, CURLOPT_TIMEOUT, 300);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$st = curl_exec($ch);
			$fd = @fopen($path, "w");
			fwrite($fd, $st);
			@fclose($fd);

			curl_close($ch);
			
			$image = new SimpleImage();
			$image->load($path);
			$image->resizeToWidth(150);
			$image->save($path);
			
			$data = file_get_contents($path);
			
			$Rec->img = base64_encode($data);
		}
		
		$Rec->name = $recipeHtml->find('.s-recipe-name')[0]->innerText();
		$Rec->descr = $recipeHtml->find('ol.instructions')[0]->innerText();
		$Rec->time = ($recipeHtml->find('p.cook-time time')[0]) ? $recipeHtml->find('p.cook-time time')[0]->innerText() : '' ;
		
		$ingr = $recipeHtml->find('tr.ingredient');
		
		for($y = 0; $y < count($ingr); $y++)
		{
			$Rec->ingredients[] = [
									'value' => $ingr[$y]->find(".name")[0]->innerText(), 
									'count' => $ingr[$y]->find('span.amount')[0]->innerText()
								];
		}
		
		$cat = $recipeHtml->find('.b-breadcrumbs a strong');
		
		for($y = 0; $y < count($y); $y++)
		{
			$Rec->categories[] = [
									'value'=>$cat[$y]->innerText()
			];
		}
		
		$Rec->run();
		
		//$recipesClasses[] = $Rec;
		
		$recipesCount++;
	}

	//$recipesCount += count($recipes);


	//next page
	$linkNext = $html->find('#link-pagination-next')[0]->href;

	if(!$linkNext)
		$paginator = false;
}
echo '<pre>';
//var_dump($recipesClasses);
echo '</pre>';
